%IMPORTVICONDATA imports comma-separated-values (csv) files obtained with
%the Vicon into Matlab. For the moment, this function is not general, and
%it assumes there is a subject Rat and a subject Treadmill.
%
%[ events, rat, treadmill ] = importViconData( path,        ...
%                                            ratName, tdmName,       ...
%                                            ratMarkers, tdmMarkers, ...
%                                            nColEv)
%
%INPUTS:
%
%path: Path and filename of the csv file (the extension .csv is required)
%ratName: String that identifies the subject Rat
%tdmName: String that identifies the subject Treadmill
%ratMarkers: Cell of strings with names of the markers in the subject Rat 
%tdmMarkers: Cell of strings with names of the markers in the subject
%Treadmill
%nColEv: number of columns that contain information about the events (if
%any). This parameter is optional. If not provided, its default value is 5.
%
%OUTPUTS:
%
%events: struct that contains information about the events (if any), as
%defined on Nexus. The field events.time is a Ne-by-1 column matrix with
%the time of occurance of the Ne events.
%rat: struct that contains information on the subject Rat. There is a field
%for each <marker>. Each rat.<marker> field is a N-by-3 matrix with
%the X,Y,Z coordinates of <marker> for N timestam. The fields rat.frame and
%rat.subframes are N-by-1 column vectors with the frame and the subframe
%of each sample in rat.<marker>.
%treadmill:struct that contains information on the subject Treadmill. There 
%is a field for each <marker>. Each treadmill.<marker> field is a N-by-3 
%matrix with the X,Y,Z coordinates of <marker> for N timestam. The fields 
%treadmill.frame and treadmill.subframes are N-by-1 column vectors with the 
%frame and the subframe of each sample in rat.<marker>.
%
%Author: Cristiano Alessandro (cristiano.alessandro@northwestern.edu)
%Date: October 19 2016
%Licence: GNU GPL

function [ events, sbjO, devO ] = importViconData ( path, varargin )
    
    events = [];
    sbjO   = [];
    devO   = [];

    nvrgs            = length(varargin);
    optargs          = {-1 -1 -1 -1};    % default values
    optargs(1:nvrgs) = varargin;         % overwrite those defined as inputs

    [sbj, sbj_mks, dev, dev_sig] = optargs{:};
    
    
    %%%%%%%%%%%%%%%%%%%%%%%% Check input parameters %%%%%%%%%%%%%%%%%%%%%%%
    
    % Trajectories
    if isempty(sbj) && ~isempty(sbj_mks)
        if isnumeric(sbj_mks) && sbj_mks==-1
            sbj_mks = {};
        else
            error('If sbj parameter is empty, sbj_mks must be empty too!');
        end
    end
    
    if ~isempty(sbj) && isempty(sbj_mks)
        error('If sbj parameter is not empty, sbj_mks cannot be empty!');
    end
    
    if iscell(sbj) && ~isempty(sbj) && iscell(sbj_mks)
        if ( length(sbj)~=1 && ~iscell(sbj_mks{1}) ) || ...
           ( length(sbj)~=1 && length(sbj)~=length(sbj_mks) )
                error('Number of sets of markers should equal number of subjects!');
        elseif length(sbj)==1 && iscell(sbj_mks{1})
            error('Wrong fromatting of input parameters');
        end            
    elseif ischar(sbj) && iscell(sbj_mks) && iscell(sbj_mks{1})
        error('Wrong fromatting of input parameters')
    end

    
    % Devices
    if isempty(dev) && ~isempty(dev_sig)
        if isnumeric(dev_sig) && dev_sig==-1
            dev_sig = {};
        else
            error('If dev parameter is empty, dev_sig must be empty too!');
        end
    end
    
    if ~isempty(dev) && isempty(dev_sig)
        error('If dev parameter is not empty, dev_sig cannot be empty!');
    end
    
    if iscell(dev) && ~isempty(dev) && iscell(dev_sig)
        if ( length(dev)~=1 && ~iscell(dev_sig{1}) ) || ...
           ( length(dev)~=1 && length(dev)~=length(dev_sig) )
                error('Number of sets of signals should equal number of devices!');
        elseif length(dev)==1 && iscell(dev_sig{1})
            error('Wrong fromatting of input parameters');
        end            
    elseif ischar(dev) && iscell(dev_sig) && iscell(dev_sig{1})
        error('Wrong fromatting of input parameters')
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Read file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % This is only good to compute nCol
    rows     = readFile(path, ',');
    nRows    = length(rows);                    % # of rows
    nCol     = cellfun( @length,rows); % # columns for each row    
    nColMax  = max(nCol);                       % max # of columns
    clear rows
    
    % Format string to read columns from file
    formatString = '%s';
    for j=1:nColMax-1
        formatString = strcat(formatString,' %s');
    end
       
    % Open file
    fileID = fopen(path);
    
    if fileID ==-1
        error('File %s not found!\n',path);
    end
    
    % Faster to read file again, than reshaping variable rows
    C = textscan(fileID,formatString, 'Delimiter',',','EmptyValue',NaN);

    fclose(fileID);

    tmp = C{1,1};

    % Start indexes
    idx_ev  = find( strcmp(C{1,1},'Events') );        % Events
    idx_tj  = find( strcmp(C{1,1},'Trajectories') );  % Trajectories
    idx_dv  = find( strcmp(C{1,1},'Devices') );       % Devices
    
    idx_all = [idx_ev; idx_tj; idx_dv];               % All    

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Events %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if ~isempty(idx_ev) 
        fprintf('Read events...');
        nColEv          = nCol(idx_ev+2);
        idx_ev          = [idx_ev findEnd(idx_ev, idx_all, nRows)]; 
        events.f        = str2double(C{1,1}{idx_ev(1)+1});
        events.colNames = cellfun(@(x)x{idx_ev(1)+2},C(1,1:nColEv),'UniformOutput',false);
        events.data     = cellfun(@(x)x(idx_ev(1)+3:idx_ev(2)), C(1,1:nColEv), ... 
                              'UniformOutput', false);
        events.time     = cellfun(@(x)str2double(x),events.data{1,4});
        fprintf('%d events found!\n',length(events.time));
    else
        fprintf('No events!\n')
        events = [];
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%% Trajectories %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if ~isempty(idx_tj) && ~isempty(sbj)
        
        fprintf('Read data...\n');
        
        nColTj = nCol(idx_tj+3);
        idx_tj = [idx_tj findEnd(idx_tj, idx_all, nRows)]; % [start end]        
        
        mkRow  = cellfun(@(x)x{idx_tj(1)+2}, C(1,1:nColTj),'UniformOutput', false);
        nMkTot = (length(mkRow)-2)/3; % xyz for each marker 
                                      % plus frame and subframe
        
        % Compute # of subjects in the file
        tmp     = cellfun(@(x)x(1:strfind(x,':')-1),mkRow,'UniformOutput',false);
        tmp     = unique(tmp);        
        allSbj  = tmp(~cellfun(@isempty, tmp));
        nAllSbj = length(allSbj);
        
        % Number of subjects to be read, and subject names
        if isnumeric(sbj) && sbj==-1
            nSbj  = nAllSbj;
            sbj_n = allSbj;
        elseif ischar(sbj) || (iscell(sbj) && length(sbj)==1)
            nSbj = 1;
            if ischar(sbj)
               sbj_n = {sbj};
            else
               sbj_n = sbj;
            end
        elseif iscell(sbj) && length(sbj)>1
            nSbj  = length(sbj);
            sbj_n = sbj;
        end    
        
        if nSbj<nAllSbj
            fprintf('WARNING: You are not reading all subjects in the file!\n');
        end
    
        % Iterate over subjects
        for i=1:nSbj
            
            sbjName = sbj_n{i};
            fprintf(' * Subject %s\n',sbjName);
            
            if ~any(strcmp(allSbj,sbjName))
                fprintf('    WARNING: not found!\n');
            else

                % frequency
                sbjO.(sbjName).f        = str2double(C{1,1}(idx_tj(1)+1));

                % frame
                sbjO.(sbjName).frame    = ...
                      cellfun(@(x)str2double(x),C{1,1}(idx_tj(1)+5:idx_tj(2)));

                % subframe
                sbjO.(sbjName).subframe = ...
                      cellfun(@(x)str2double(x),C{1,2}(idx_tj(1)+5:idx_tj(2)));

                % Find all marker names for subject i
                tmpStr = [sbjName ':'];
                tmp    = cellfun(@(x)x(strfind(x,tmpStr)+length(tmpStr):end), ...
                                mkRow,'UniformOutput',false);
                allMks  = tmp(~cellfun(@isempty, tmp));

                if isnumeric(sbj_mks) && sbj_mks==-1          % read all markers
                    mks = allMks;            
                elseif ischar(sbj_mks)                        % Only one marker
                    mks = {sbj_mks};
                elseif iscell(sbj_mks) && ~iscell(sbj_mks{1}) % One set of markers (one subject)
                    mks = sbj_mks;
                elseif iscell(sbj_mks) && iscell(sbj_mks{1})  % Different sets of markers (one for subject)
                    mks = sbj_mks{i};
                end

                if length(mks)<length(allMks)
                    fprintf('    WARNING: You are not reading all the markers!\n');
                end

                % Iterate over markers
                for j=1:length(mks)                  
                    mkName = [sbjName ':' mks{j}];                    
                    idxCol = find(strcmp(mkRow,mkName));
                    if isempty(idxCol)
                        fprintf('    WARNING: Marker %s not found!\n',mkName);
                    else
                        sbjO.(sbjName).(sprintf('%s',mks{j})) = cell2mat( ...
                            cellfun( @(x)str2double(x((idx_tj(1)+5:idx_tj(2)))), ...
                            C(1,idxCol:idxCol+2), 'UniformOutput',false));

                        fprintf('    Marker %s...OK\n',mkName);
                    end
                end % markers
            
            end
            
        end % subjects
        
    else
        fprintf('No trajectory data, or you specified no subjects!\n');
        sbjO = [];
    end
        
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Devices %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if ~isempty(idx_dv) && ~isempty(dev)               
       
       % find all devices (names)
       idx_nx = idx_all( idx_all>idx_dv(1) );       
       if isempty(idx_nx)         % Devices are in the last section of csv
           idx_nx = nRows;
           idx_rg = idx_dv(1):idx_nx;
       else
           idx_nx = idx_nx(1);
           idx_rg = idx_dv(1):idx_nx-1;
       end             
                           
       idx_dvAll =  strcmp(C{1,1}(idx_rg),'') & strcmp(C{1,2}(idx_rg),'') & ...
                    strcmp(C{1,4}(idx_rg),'');
       idx_dvAll = idx_rg(idx_dvAll);       % Indexes of all devices in csv
       devAll    = C{1,3}(idx_dvAll);       % Names of all devices in csv
       nDevAll   = length(devAll);
       
       
       if isnumeric(dev) && dev==-1                          % Read all devices
          dev_n = devAll;
          nDev  = nDevAll;
       elseif ischar(dev) || (iscell(dev) && length(dev)==1) % One device
          nDev = 1;
          if ischar(dev)
             dev_n = {dev};
          else
             dev_n = dev;
          end
       elseif iscell(dev) && length(dev)>1                   % Several devices
          nDev  = length(dev);
          dev_n = dev;
       end
       
       if nDev<nDevAll
            fprintf('WARNING: You are not reading all devices in the file!\n');
       end
       
       % Iterate over devices
       for i=1:nDev
           
          dev_c = dev_n{i};
          
          switch dev_c
              case 'analog'
                  fprintf ('Not implemented yet! Sorry...\n');
                  str_o = 'analog';
                  str   = '';       % I do not know (to be implemented)
              case {'emg','Imported Analog EMG - Voltage'}                  
                  str   = 'Imported Analog EMG - Voltage';
                  str_o = 'emg';
              case 'Analog EMG - Voltage'
                  str   = 'Analog EMG - Voltage';
                  str_o = 'emg';
              otherwise
                  fprintf('Wrong device name: %s. Use one of the following: emg\n', dev_c);
                  str = 'wrong';
          end          
                    
          fprintf(' * Device %s\n',dev_c);   
          
          if ~any(strcmp(devAll,str))
              fprintf('    WARNING: not found!\n');
          else
              
              devName = str_o;
              
              % Start and end of current device
              idx_st = idx_dvAll( strcmp(devAll,str) ) + 3;
              idx_ed = findEnd (idx_st, idx_all, idx_nx);
              nColDv = nCol(idx_st-2);
          
              % frequency
              devO.(devName).f        = str2double(C{1,1}(idx_dv(1)+1));

              % frame
              devO.(devName).frame    = ...
                  cellfun(@(x)str2double(x),C{1,1}(idx_st:idx_ed));

              % subframe
              devO.(devName).subframe = ...
                  cellfun(@(x)str2double(x),C{1,2}(idx_st:idx_ed));

              sigRow  = cellfun(@(x)x{idx_st-2}, C(1,1:nColDv),'UniformOutput', false);
              allSig  = sigRow(3:end);

              if isnumeric(dev_sig) && dev_sig==-1          % read all signals
                  sig = allSig;
              elseif ischar(dev_sig)                        % Only one signal
                  sig = {dev_sig};
              elseif iscell(dev_sig) && ~iscell(dev_sig{1}) % One set of signals (one device)
                  sig = dev_sig;
              elseif iscell(dev_sig) && iscell(dev_sig{1})  % Different sets of signals (one for device)
                  sig = dev_sig{i};
              end

              if length(sig)<length(allSig)
                  fprintf('    WARNING: You are not reading all the signals!\n');
              end

              % Iterate over device signals
              for j=1:length(sig)
                  sigName = sig{j};
                  idxCol = find( strcmp(sigRow,sigName) );
                  if isempty(idxCol)
                      fprintf('    WARNING: Signal %s not found!\n',sigName);
                  else
                      if ~isempty(str2num(sigName))
                          sigName = ['ch' sigName];
                      end
                      devO.(devName).(sigName) = cell2mat( ...
                          cellfun( @(x)str2double(x(idx_st:idx_ed)), ...
                          C(1,idxCol), 'UniformOutput',false));

                      fprintf('    Signal %s...OK\n',sigName);
                  end
              end % signal
          end
       end % device
       
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('done!\n');

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function computes the final index of the section of the csv file,
% that begin with index "start". It requires a vector "all" will all the
% starting indexes, and a variable "nRow" with the number of rows in the
% file.
function idx_end = findEnd (start, all, nRow)
    
    if isempty(start)
        error('Start index is empty!')
    end

    if start==max(all)      
        idx_end = nRow;                      % If it is the last index
    else
        idx_end = min( all(all>start) )-1;   % find the next index - 1
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read the csv filename row-wise.
function rows = readFile(filename, delimiter)

    fid = fopen(filename);

    if fid ==-1
        error('File %s not found!\n',filename);
    end

    lineIndex = 1;                
    nextLine = fgetl(fid);        % Read first line from file
    while ~isequal(nextLine,-1)   % Loop while not at the end of the file
        if ~isempty(nextLine)
            if strcmp(nextLine(end),delimiter)
                nextLine=[nextLine 'NaN']; %%%%%%%%%%%%%%%%%%%%%%% THIS IS WRONG!!!
            end
            lineArray{lineIndex} = nextLine;    % Add the line to the cell array            
            lineIndex            = lineIndex+1; % Increment the line index
        end
        nextLine = fgetl(fid);      % Read the next line from the file
    end    
    fclose(fid);

    rows = cellfun( @(x)(textscan(x,'%s','Delimiter',delimiter, ...
                                        'EmptyValue',NaN))', lineArray);  
    rows = rows';
end
% alfa = COMPUTEANGLE ( p1, p2, p3 ) computes the angle between two vectors (in 
% degrees). The vectors originate from the point p2 and point to p1 and p3. 
% The points p1, p2 and p3 are expressed in a global frame of reference.
%
%INPUTS:
%
%p1, p2, p3: These variables can be bi-dimensional vectors,
%three-dimensional vectors, N-by-2 matrices or N-by-3 matrices. 
%In the first two cases, the function computes the angle (in radians) 
%between the vectors (2D or 3D). If p1, p2 and p3 are N-by-2 (or N-by-3) 
%matrices, the function computes the angles between each row of p1-p2 and 
%the corresponding one of p3-p2. These three variables must have the same
%size, ie. size(p1)==size(p2)==size(p3).
%
%OUTPUTS:
%
%alfa: angle between the vectors p1-p2 and p3-p2 (degrees)
%
%Author: Cristiano Alessandro (cristiano.alessandro@northwestern.edu)
%Date: April 07 2016
%Licence: GNU GPL

%% Copyright (c) 2016 Cristiano Alessandro <cristiano.alessandro@northwestern.edu>
%
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program. If not, see <http://www.gnu.org/licenses/>.

function alfa = computeAngle( p1, p2, p3, varargin )

   nvrgs            = length(varargin);
   optargs          = {[],false};       % default values
   optargs(1:nvrgs) = varargin;         % overwrite those defined as inputs

   [n,flagInvert]   = optargs{:};       % n: normal direction of the plane on which the points lie
                                        % flagInvert: false for clockwise from p2p1 to p2p3                                           
       
                                        
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Checks %%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   % Check consistency
   if min(size(p1))==1
       
       % One normal vector only
       if ~isempty(n) && min(size(n))~=1
           error('You should provide only one normal vector!');
       end
       
       p1 = p1(:); % point 1
       p2 = p2(:); % point 2
       p3 = p3(:); % point 3 
       
       p1_sM = max(size(p1));
       p2_sM = max(size(p2));
       p3_sM = max(size(p3));
       
       if ~(p1_sM==2 && p2_sM==2 && p3_sM==2) && ...
            ~(p1_sM==3 && p2_sM==3 && p3_sM==3)
           
           error('Points must have the same dimensions (2 or 3)!');
       end
       
       % Transform into three dimensional vectors
       if p1_sM==2
          p1 = [p1;0];
          p2 = [p2;0];
          p3 = [p3;0];
       end
       
   else
       
       % One normal vector for each point
       if ~isempty(n) && max(size(n))~=max(size(p1))
           error('You must provide a normal for each point');
       end
       
       if ~(size(p1,2)==2 && size(p2,2)==2 && size(p3,2)==2) && ...
            ~(size(p1,2)==3 && size(p2,2)==3 && size(p3,2)==3)
           
           error(['All input variables must have the same dimension. ' ...
                  'They can be either N-by-3 or N-by-2 matrices']);
       end
       
       % Transform into three dimensional vectors
       nPt = size(p1,1);     % Number of points
       if size(p1,2)==2
           p1  = [p1 zeros(nPt,1)];
           p2  = [p2 zeros(nPt,1)];
           p3  = [p3 zeros(nPt,1)];
       end
       
   end
      
   % Check if provided normal is perpendiculrar to all input points
   if ~isempty(n)

      if min(size(p1))==1                
        
        n = n(:);
        if size(p1,1)~=1
            p1 = p1';
            p2 = p2';
            p3 = p3';
        end

      else        
        
        n = reshape(n,max(size(n)),3);  % N-by-3 matrix of normals
        n = n';                         % 3-by-N matrix of normals
          
      end
      
      P     = [p1; p2; p3];
      prj_n = P*n;            % Projection of point onto the normal
      
      if any(prj_n(:))
         error('The provided normal is not orthogonal to the points');
      end
      
      if min(size(p1))>1
          n = n';           % N-by-3 matrix of normals
      end
      
   end
     
   % Input paramenter consistency
   if isempty(n) && flagInvert==true
       flagInvert = false;
       fprintf('WARNING: You did not provide the optional 3rd input argument!\n');
       fprintf('The notion of clockwise/counterclockwise is ill \n');
       fprintf('posed if no normal direction is defined. If you need \n');
       fprintf('clockwise angle (4th input argument equal to TRUE) \n');
       fprintf('you cannot leave the 3rd argument empty.\n');
       fprintf('I ignore the 4th argument!\n');
   end
      
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%

   if min(size(p1))==1                                
        
        p1 = p1(:); % point 1
        p2 = p2(:); % point 2
        p3 = p3(:); % point 3                
        
        % Transform into three dimensional vectors
        if p1_sM==2
           p1 = [p1;0];
           p2 = [p2;0];
           p3 = [p3;0];
        end
        
        v1 = p1-p2; % vector 1
        v2 = p3-p2; % vector 2                
        
        v1_nl = v1./norm(v1);  % normalize
        v2_nl = v2./norm(v2);  % normalize
        
        % Angles in the range [0,pi]. That is because 
        % the cross product to compute n will generate two
        % different directions depending on whether the angle between the
        % vectors is higher or lower than pi.
        if isempty(n)
                        
            if dot(v1_nl,v2_nl)==1          % Parallel vectors                
                sin12 = 0;
                cos12 = 1;                
            elseif dot(v1_nl,v2_nl)==-1     % Parallel vectors                
                sin12 = 0;
                cos12 = -1;                                
            else                            % Non-parallel vectors                
                n = cross(v1_nl,v2_nl);
            end
    
        end                            
        
        % Either the vectors are not parallel, or n was provided as input       
        if ~isempty(n)
            
            % Normalize
            n = n(:)./norm(n);

            % Orthonormal basis (for the plane onto which the vectors lie)
            e1 = v1_nl;               % versor 1
            if flagInvert
                e2 = cross(-n,e1);    % versor 2 (clockwise angle)
            else
                e2 = cross(n,e1);     % versor 2 (counterclockwise angle)
            end
            e2 = e2./norm(e2);

            sin12 = dot(e2,v2_nl);
            cos12 = dot(e1,v2_nl);
            
        end        
        
   else
                
        nPt   = size(p1,1);     % Number of points
        idxNP = 1:nPt;          % Indexes of non-parallel vectors
       
        % Transform into three dimensional vectors
        if size(p1,2)==2           
           p1  = [p1 zeros(nPt,1)];
           p2  = [p2 zeros(nPt,1)];
           p3  = [p3 zeros(nPt,1)];
        end
        
        v1 = p1-p2; % vectors 1
        v2 = p3-p2; % vectors 2
        
        v1_n = norm_cw(v1);
        v2_n = norm_cw(v2);

        v1_nl = v1./repmat(v1_n,1,size(v1,2)); % normalize
        v2_nl = v2./repmat(v2_n,1,size(v2,2)); % normalize          
        
        % Angles in the range [0,pi]. That is because 
        % the cross product to compute n will generate two
        % different directions depending on whether the angle between the
        % vectors is higher or lower than pi.
        if isempty(n)
            
            prjV1V2  = dot(v1_nl,v2_nl,2);
            idxPrj1  = prjV1V2==1;              % Parallel (same direction)
            idxPrjM1 = prjV1V2==-1;             % Parallel (opposite direction)
            idxNP    = ~idxPrj1 & ~idxPrjM1;    % Non-parallel
            
            if any(idxPrj1) || any(idxPrjM1)    % Parallel vectors
            
                sin12(idxPrj1,1) = 0;
                cos12(idxPrj1,1) = 1;

                sin12(idxPrjM1,1) = 0;
                cos12(idxPrjM1,1) = -1;
            
            end
            
            n  = cross(v1_nl,v2_nl,2);          % Non-parallel vectors            
    
        end              
               
        n   = reshape(n,nPt,3);
        n_n = norm_cw(n);
        n   = n./repmat(n_n,1,size(n,2)); % normalize
        
        % Orthonormal basis (for the plane onto which the vectors lie)
        e1 = v1_nl;                 % versors 1
        if flagInvert
            e2 = cross(-n,e1,2);    % versor 2 (clockwise angle)
        else
            e2 = cross(n,e1,2);     % versor 2 (counterclockwise angle)
        end
        e2_n = norm_cw(e2);
        e2   = e2./repmat(e2_n,1,size(e2,2)); % normalize
                       
        sin12(idxNP,1) = dot(e2(idxNP,:),v2_nl(idxNP,:),2);
        cos12(idxNP,1) = dot(e1(idxNP,:),v2_nl(idxNP,:),2);
                        
   end
        
   alfa = atan2(sin12,cos12);    % radians [-pi;pi]
   alfa = mod(alfa,2*pi);        % radians [0;2*pi]
   alfa = 180/pi*alfa;           % degrees [0;360]
   
end
function [RT_g2t, RT_t2g] = computeTreadmillFoR (markers)

    dat_m = mean(markers,'omitnan');
    tdm   = (reshape(dat_m,3,4))';
    
    % Fit a plane to the treadmill mean marker positions
    [n,d,p] = fitPlane (tdm);
    
    % Project markers onto the sagittal plane
    [tdm_proj_g, tdm_proj] = project_v2p(tdm,n,p);

    % To check if these points are on the plane (err should be zero)
    err = tdm_proj_g*n'-d;

    % Origin (corresponding to marker 4)
    o = tdm_proj_g(4,:);

    % Y-axis
    y = tdm_proj_g(2,:)-o;
    y = y./repmat(norm_cw(y),1,size(y,2));

    % X-axis
    x = n;
    %x = -n;
    
    % Z-axis
    z = cross(x,y);
    
    % Check axes (z should point in the opposite direction than vtmp)
    vtmp = tdm_proj_g(1,:) - o;
    vpj  = vtmp*z';
    if vpj>0
        x = -x;
        z = cross(x,y);
    end

    R_g2t = [x' y' z']; % Rotation of the treadmill coordinate system wrt to 
                        % global. Multiplying by a point expressed in the   
                        % treadmill FoR one obtains the global coordinate of the 
                        % point.

    R_t2g = R_g2t';     % Rot. of the global coordinate system wrt to treadmill 

    RT_g2t = [R_g2t      o'; ...        % Rototransl. treadmill FoR wrt global
              zeros(1,3) 1];

    RT_t2g = [R_t2g      -R_t2g*o'; ... % Rototransl. global FoR wrt treadmill
              zeros(1,3) 1];     

    % To verify the axes are all orthogonal (xyz should be an identity)
    xyz = R_g2t*R_g2t';
    

end
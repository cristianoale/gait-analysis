%clear all
close all

filename      = './demo_data/post5_s15_d30_01.csv';

ratName       = 'skeleton';
ratMarkers    = {'pelvis1','pelvis2','pelvis3','femur1','femur2', ...
                 'femur3','tibia1','tibia2','foot12','foot22'};
ratEmg        = {'1','2','3','4','5','6','7','8'};

treadmillName    = 'treadmill';
treadMillMarkers = {'treadmill1','treadmill2','treadmill3','treadmill4'};


                                        
%% Import all subjects, all markers, all devices
[ events, subjects, devices ] = importViconData(filename);
                              

%% Import one subject, and all its markers, all devices
[ events, subjects, devices ] = importViconData(filename, 'treadmill');


%% Import one subject, some of its markers (one of which does not exist), all devices
[ events, subjects, devices ] = importViconData(filename, ...
                                 'treadmill','treadmill1');
           
                              
%% Import one subject, some of its markers (one of which does not exist), all devices
[ events, subjects, devices ] = importViconData(filename, ...
                                 'treadmill',{'treadmill1','treadmill5','treadmill4'});
                            
                              
%% Import one subject, some of its markers (one of which does not exist), all devices
[ events, subjects, devices ] = importViconData(filename, ...
                                 {'treadmill'},{'treadmill1','treadmill5','treadmill4'});
                                                           

%% Import some subjects, some markers, all devices
[ events, subjects, devices ] = importViconData(filename,        ...
                                 {'treadmill','skeleton'},       ...
                                 {  {'treadmill1','treadmill3'}, ...
                                    {'femur3','tibia1','foot'}   ...
                                 });

                             
%% Does not import subjects, but import devices
[ events, subjects, devices ] = importViconData(filename,[]);


%% Does not import subjects, import some devices (all signals)
[ events, subjects, devices ] = importViconData(filename,[],[],'emg');     


%% Does not import subjects, import some devices (some signals)
[ events, subjects, devices ] = importViconData(filename,[],[],'emg',{'3','7','8'});     

    
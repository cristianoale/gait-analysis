# README #

Library to:

Preprocess of Vicon data, computation of joint angles from 3D marker trajectories, segmentation of raw locomotion data into gait cycles and stance/swing phases, computation of gait features, EMGs preprocessing (filtering, rectification, envelopes), basic signal processing functionalities, utilities to visualize locomotion data in animated stick figures.


### Contributors ###

Cristiano Alessandro, PhD
function idx = detectPeaks ( v, epsVD )        
    
    v       = v(:); 
    vD      = diff(v);    
       
    % Get the indexes of the values of vD closer to zero
    idxZ   = find(abs(vD)<epsVD);	    
    k      = 1;        
    idx    = [];
    
    for j=1:length(idxZ)-1

        idxSt = idxZ(j);
        idxEd = idxZ(j+1);
        
        if idxEd-idxSt==1
            if (vD(idxEd)>0 && vD(idxSt)<0) || (vD(idxEd)<0 && vD(idxSt)>0)
                idx(k,1) = idxEd;
                k=k+1;
            end
        end
        
    end        

end


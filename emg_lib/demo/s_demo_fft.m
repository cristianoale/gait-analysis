f1 = 200;   % acquisition freq signal 1
f2 = 500;   % acquisition freq signal 1

fSig1 = 2;  % frequency component 1
fSig2 = 7;  % frequency component 2

tMax = 30;  % time of simulation

t1 = linspace(0,tMax,tMax*f1);   % time vector signal 1
t2 = linspace(0,tMax,tMax*f2);   % time vector signal 2

% Signal 1
s1  = sin(2*pi*fSig1*t1) + sin(2*pi*fSig2*t1);
s1n = s1 + 2*randn(size(t1));

% Signal 2
s2 = sin(2*pi*fSig1*t2) + sin(2*pi*fSig2*t2);
s2n = s2 + 2*randn(size(t2));

% spectrum signal 1
figure
plotSpectrum(s1n)

% spectrum signal 2
figure
plotSpectrum(s2n)

% Both spectra show the correct frequency components
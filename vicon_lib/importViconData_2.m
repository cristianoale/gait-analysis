%IMPORTVICONDATA imports comma-separated-values (csv) files obtained with
%the Vicon into Matlab. For the moment, this function is not general, and
%it assumes there is a subject Rat and a subject Treadmill.
%
%[ events, rat, treadmill ] = importViconData( path,        ...
%                                            ratName, tdmName,       ...
%                                            ratMarkers, tdmMarkers, ...
%                                            nColEv)
%
%INPUTS:
%
%path: Path and filename of the csv file (the extension .csv is required)
%ratName: String that identifies the subject Rat
%tdmName: String that identifies the subject Treadmill
%ratMarkers: Cell of strings with names of the markers in the subject Rat 
%tdmMarkers: Cell of strings with names of the markers in the subject
%Treadmill
%nColEv: number of columns that contain information about the events (if
%any). This parameter is optional. If not provided, its default value is 5.
%
%OUTPUTS:
%
%events: struct that contains information about the events (if any), as
%defined on Nexus. The field events.time is a Ne-by-1 column matrix with
%the time of occurance of the Ne events.
%rat: struct that contains information on the subject Rat. There is a field
%for each <marker>. Each rat.<marker> field is a N-by-3 matrix with
%the X,Y,Z coordinates of <marker> for N timestam. The fields rat.frame and
%rat.subframes are N-by-1 column vectors with the frame and the subframe
%of each sample in rat.<marker>.
%treadmill:struct that contains information on the subject Treadmill. There 
%is a field for each <marker>. Each treadmill.<marker> field is a N-by-3 
%matrix with the X,Y,Z coordinates of <marker> for N timestam. The fields 
%treadmill.frame and treadmill.subframes are N-by-1 column vectors with the 
%frame and the subframe of each sample in rat.<marker>.
%
%Author: Cristiano Alessandro (cristiano.alessandro@northwestern.edu)
%Date: April 13 2016
%Licence: GNU GPL


function [ events, rat, treadmill ] = importViconData_2( path,        ...
                                            ratName, tdmName,       ...
                                            ratMarkers, tdmMarkers, ...
                                            nColEv)
                                        
%function [ events, sbj, dev ] = importViconData ( path, sbj, sbj_mks, dev, dev_sig, nColEv )

    rows  = readFile(path, ',');

    nCol  = max(cellfun( @(x)length(x{1}),rows));
    nRows = length(rows);

    if nargin<6
        nColEv = 5;          % #columns events
    end    

    % Start indexes
    idx_ev  = find( cellfun(@(x)strcmp(x{1},'Events'),rows) );       % Events
    idx_tj  = find( cellfun(@(x)strcmp(x{1},'Trajectories'),rows) ); % Trajectories
    idx_dv  = find( cellfun(@(x)strcmp(x{1},'Devices'),rows) );      % Devices
    idx_all = [idx_ev; idx_tj; idx_dv];                              % All

    % There must be trajectory data
    if isempty(idx_tj)
        error('No trajectory data!');
    end
    idx_tj = [idx_tj findEnd(idx_tj, idx_all, nRows)]; % [start end]

    % If there are events
    if ~isempty(idx_ev)        
        idx_ev          = [idx_ev findEnd(idx_ev, idx_all, nRows)]; 
        events.f        = str2double(rows{idx_ev(1)+1});
        events.colNames = rows{idx_ev(1)+2}';
                
        tmpDat      = cellfun(@(x)x,rows(idx_ev(1)+3:idx_ev(2)), ...
                                'UniformOutput', false);
        events.data = cat(2,tmpDat{:})';
        
        events.time     = str2double(events.data(:,4));
        fprintf('%d events found!\n',length(events.time));
    else
        fprintf('No events!\n')
        events = [];
    end

    % Read trajectory data
    fprintf('Read data...');
    
    rat.f        = str2double(rows{idx_tj(1)+1});   
    rat.frame    = str2double(cellfun(@(x)x{1},rows(idx_tj(1)+5:idx_tj(2)), ...
                                'UniformOutput', false));                            
    rat.subframe = str2double(cellfun(@(x)x{2},rows(idx_tj(1)+5:idx_tj(2)), ...
                                'UniformOutput', false));                                
    
    treadmill. f        = rat.f;
    treadmill. frame    = rat.frame;
    treadmill. subframe = rat.subframe;

    %%% ADD A CHECK THAT ALL THE DATA ARE BEING READ!
    
    % Seek treadmil markers columns
    for j=1:length(tdmMarkers)
       mkName = [tdmName ':' tdmMarkers{j}];              
       mkRow  = rows{idx_tj(1)+2};
       idxCol = find(cellfun(@(x)strcmp(x,mkName),mkRow));
       if isempty(idxCol)
           error('Marker %s not found!',mkName);
       end
       treadmill.(sprintf('%s',tdmMarkers{j})) = cell2mat( ...
           cellfun(@(x)str2double( cat(1,x(idxCol:idxCol+2))' ), ...
           rows(idx_tj(1)+5:idx_tj(2)), 'UniformOutput', false));
       
                 
     %%% CREATE A VARIABLE FOR EACH SUBJECT, WITH FRAME AND SUBFRAMES TOO
                 
    end

    % Seek rat markers columns
    for j=1:length(ratMarkers)
       mkName = [ratName ':' ratMarkers{j}];
       mkRow  = rows{idx_tj(1)+2};       
       idxCol = find(cellfun(@(x)strcmp(x,mkName),mkRow));
       if isempty(idxCol)
           error('Marker %s not found!',mkName);
       end
       
       rat.(sprintf('%s',ratMarkers{j})) = cell2mat( ...
           cellfun(@(x)str2double( cat(1,x(idxCol:idxCol+2))' ), ...
           rows(idx_tj(1)+5:idx_tj(2)), 'UniformOutput', false));
       
    end
    
    %%% CREATE FIELD MARKERS, WITH SUBFIELDS
    %%% CREATE FIELD EMG, WITH SUBFIELDS
    %%% ONE SHOULD KEEP TRACK OF WHICH SUBJECT IS ASSOCIATED WITH THE
    %%% ANALOG SIGNALS

    fprintf('done!\n');

end

function idx_end = findEnd (start, all, nRow)
    
    if isempty(start)
        error('Start index is empty!')
    end

    if start==max(all)      
        idx_end = nRow;                      % If it is the last index
    else
        idx_end = min( all(all>start) )-1;   % find the next index - 1
    end
    
end

function rows = readFile(filename, delimiter)

    fid = fopen(filename);

    if fid ==-1
        error('File %s not found!\n',filename);
    end

    lineIndex = 1;                
    nextLine = fgetl(fid);        % Read first line from file
    while ~isequal(nextLine,-1)   % Loop while not at the end of the file
        if ~isempty(nextLine)
            if strcmp(nextLine(end),delimiter)
                nextLine=[nextLine 'NaN'];
            end
            lineArray{lineIndex} = nextLine;    % Add the line to the cell array            
            lineIndex            = lineIndex+1; % Increment the line index
        end
        nextLine = fgetl(fid);      % Read the next line from the file
    end
    %lineArray = lineArray(1:end-1); % Eliminate last line (empty)
    fclose(fid);

    rows = cellfun( @(x)(textscan(x,'%s','Delimiter',delimiter, ...
                                        'EmptyValue',NaN))', lineArray);  
    rows = rows';
end
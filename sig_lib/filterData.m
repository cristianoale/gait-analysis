%TODO: comment

function data_filt = filterData(data, cutoff, fs, order, varargin)
        
    Wn        = cutoff(:) ./ (fs/2);    % Normalized cut-off frequency    
    
	 if nargin>4       
       ftype = varargin{1};
    elseif length(cutoff)==2
       ftype = 'bandpass';
    else
       ftype = 'low';
    end
    
    [b,a]     = butter(order, Wn, ftype);
    data_filt = filtfilt(b,a,data); % Filtered data
    
    
    % The butterworth filter accepts cut-off frequencies normalized wrt 
    % half the sampling rate of the signal fs. The reason can be explained 
    % as follows. It does not make sense to apply a low-pass filter with
    % cut-off higher than the bandwith B of the original signal, however
    % cutt-off frequencies in the range [0;B] should be allowed. 
    % Assuming that the signal has been sampled properly (i.e. at
    % a minimum sampling frequency equal to the Nyquist frequency fs=fN=2B),
    % then B can be estimated as B=fs/2. If the sampling frequency is
    % actually higher than Nyquist (fs>fN), then fs/2 is higher than B. In
    % other words, taking the maximum cut-off frequency of the filter equal
    % to fN, allows to implement all possible meaningful (i.e. with cut-off
    % lower or equal then B) even in the extreme case in which the sampling
    % frequency is the minimum possible (fN).
    

end

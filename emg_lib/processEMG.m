% processEMG filters, rectifies and computes envelopes from raw EMG signals
%
% [ emg_f, emg_r, emg_e ] = processEMG (emg , fs, fOrd_ma, band_ma, ...
%                                                 fOrd_e,  fc_e) 
%
%INPUTS:
%
%emg: N-by-M matrix with N samples of M raw emg signals
%fs: sampling frequency
%fOrd_ma: order of the batterworth bandpass filter to remove artifacts
%band_ma: band of the  batterworth bandpass filter to remove artifacts
%fOrd_e: order of the batterworth lowpass filter to compute envelopes
%fc_e: cutoff frequency of the batterworth lowp filter to compute envelopes
%
%OUTPUTS:
%
%emg_f: filtered emg signals (no motion artifacts)
%emg_r: rectified emg signals (after filtering)
%emg_e: emg envelopes
%
%Author: Cristiano Alessandro (cristiano.alessandro@northwestern.edu)
%Date: June 07 2016
%Licence: GNU GPL

%% Copyright (c) 2016 Cristiano Alessandro <cristiano.alessandro@northwestern.edu>
%
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program. If not, see <http://www.gnu.org/licenses/>.


function [ emg_f, emg_r, emg_e ] = processEMG( emg , fs, ...
                                               fOrd_ma, band_ma, ...
                                               fOrd_e, fc_e)


   % remove motion artifacts and noise (bandpass filter)
   emg_f = filterData(emg, band_ma, fs, fOrd_ma, 'bandpass');

   % rectify and compute envelopes
   emg_r = abs(emg_f);                                   % rectify
   emg_e = filterData(emg_r, fc_e, fs, fOrd_e, 'low');   % envelopes

end


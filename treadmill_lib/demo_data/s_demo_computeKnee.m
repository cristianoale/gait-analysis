%load ('./aft_s10_d30_01.mat');

%%

femurL  = 40*4; % Length of the femur [mm]
tibiaL  = 45*4; % Length of the tibia [mm]

hip     = rat_tm.hip_center;
kneeMk  = rat_tm.knee;
heel    = rat_tm.heel;

knee = computeKnee ( hip, kneeMk, heel, femurL, tibiaL );

%% plot  

plot3(dat(:,1),dat(:,2),dat(:,3),'o')
hold on
plot3(dat(1,1),dat(1,2),dat(1,3),'x')
grid on

quiver3(o(1),o(2),o(3),x(1)*20,x(2)*20,x(3)*20)
quiver3(o(1),o(2),o(3),y(1)*20,y(2)*20,y(3)*20,'color','red')
quiver3(o(1),o(2),o(3),z(1)*20,z(2)*20,z(3)*20,'color','green')

plot3(knee(1),knee(2),knee(3),'ro')


%% animation

% Project rat markers into yz plane
rat_tm_yz = {rat_tm.hip_top(:,[2,3])    ...
             rat_tm.hip_bottom(:,[2,3]) ...
             rat_tm.hip_center(:,[2,3]) ...
             rat_tm.knee(:,[2,3])       ...
             rat_tm.heel(:,[2,3])       ...
             knee(:,[2,3])              ...
             rat_tm.hip_center(:,[2,3]) ...
             knee(:,[2,3])              ...
             rat_tm.heel(:,[2,3])       ...
             rat_tm.metatarsal(:,[2,3]) ...
             rat_tm.phalanx(:,[2,3]) };

% Project treadmill markers into yz plane          
tdm_tm_yz = tdm_tm(:,[2,3]);

% Find min and max for the plot
xMin = cell2mat(cellfun(@(x)min(x(:,1)),rat_tm_yz,'UniformOutput', false));
xMin = min([xMin min(tdm_tm_yz(:,1))]);

xMax = cell2mat(cellfun(@(x)max(x(:,1)),rat_tm_yz,'UniformOutput', false));
xMax = max([xMax max(tdm_tm_yz(:,1))]);

yMin = cell2mat(cellfun(@(x)min(x(:,2)),rat_tm_yz,'UniformOutput', false));
yMin = min([yMin min(tdm_tm_yz(:,2))]);

yMax = cell2mat(cellfun(@(x)max(x(:,2)),rat_tm_yz,'UniformOutput', false));
yMax = max([yMax max(tdm_tm_yz(:,2))]);

% create figure
hf = figure;

% Plot treadmill
plot(tdm_tm_yz(:,1),tdm_tm_yz(:,2),'o')
axis([xMin xMax yMin yMax])
grid on
hold on

segmentGaitAnimation(rat_tm_yz, 100, 'animate');
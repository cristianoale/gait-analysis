load ./demo/aft_s10_d30_01.mat

%% Connect markers in a sequence (within a cell)

% Project rat markers into yz plane
rat_tm_yz = {rat_tm.hip_top(:,[2,3])    ...
             rat_tm.hip_bottom(:,[2,3]) ...
             rat_tm.hip_center(:,[2,3]) ...
             rat_tm.knee(:,[2,3])       ...
             rat_tm.heel(:,[2,3])       ...
             rat_tm.metatarsal(:,[2,3]) ...
             rat_tm.phalanx(:,[2,3]) };

% Project treadmill markers into yz plane          
tdm_tm_yz = tdm_tm(:,[2,3]);

% Find min and max for the plot
xMin = cell2mat(cellfun(@(x)min(x(:,1)),rat_tm_yz,'UniformOutput', false));
xMin = min([xMin min(tdm_tm_yz(:,1))]);

xMax = cell2mat(cellfun(@(x)max(x(:,1)),rat_tm_yz,'UniformOutput', false));
xMax = max([xMax max(tdm_tm_yz(:,1))]);

yMin = cell2mat(cellfun(@(x)min(x(:,2)),rat_tm_yz,'UniformOutput', false));
yMin = min([yMin min(tdm_tm_yz(:,2))]);

yMax = cell2mat(cellfun(@(x)max(x(:,2)),rat_tm_yz,'UniformOutput', false));
yMax = max([yMax max(tdm_tm_yz(:,2))]);

% create figure
hf = figure;

% Plot treadmill
plot(tdm_tm_yz(:,1),tdm_tm_yz(:,2),'o')
axis([xMin xMax yMin yMax])
grid on
hold on
          
checkGaitSegmentation(fStrike, fOff, rat_tm_yz, rat.f);
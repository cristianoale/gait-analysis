function s = similarity ( x , y , method )     

   if isempty(x) || isempty(y) || all(isnan(x(:))) || all(isnan(y(:)))
       s = NaN;
   else              

       switch method

          case {'rmsd'}
             
             if size(x,2)>1
                 m_x = mean(x,2,'omitnan');
                 m_y = mean(y,2,'omitnan');
                 s   = sqrt( sumsqr(m_x-m_y)/length(m_x) ); % 2nd raw moment of difference
             else
                 s = sqrt( sumsqr(x-y)/length(x) ); % 2nd raw moment of difference
             end

          case {'rmsd_n'}

             if min(size(x))==1 || min(size(y))==1
                 fprintf('WARNING: you are not providing the full population');
             end

             m_x   = mean(x,2,'omitnan');
             m_y   = mean(y,2,'omitnan');
             rmsd  = sqrt( sumsqr(m_x-m_y)/length(m_x) ); % 2nd raw moment of difference
             var_x = mean( var(x,0,2,'omitnan') );        % mean variance of x
             var_y = mean( var(y,0,2,'omitnan') );        % mean variance of y
             pStd  = sqrt( mean([var_x,var_y]) );         % pooled standard deviation

             s = rmsd / pStd;

          case {'vaf_mod'}
             SS_xy = sumsqr(x-y)/length(x); % mean sum of squared differences
             var_x = var(x);
             var_y = var(y);
             mVar  = mean([var_x var_y]);

             s = 1 - SS_xy/mVar;

             %SS_x  = sumsqr(x-mean(x));
             %SS_y  = sumsqr(y-mean(y));
             %mSS   = mean([SS_x,SS_y]);
             %s = 1 - SS_xy/mSS;

          otherwise
             error('Wrong similarity measure');

       end   
   
   end

end


function plotEMG( t, emg, varargin)

   if nargin>2
      names = varargin{1};         
   else
      names = [];
   end
   
   nEMG = size(emg,2);
      
   for j=1:nEMG
      subplot(nEMG,1,j)
      
      % raw
      plot(t,emg(:,j))
      axis tight
      xlabel('time [s]')
      if ~isempty(names)
         ylabel(names{j})
      end
      grid on
      
      % filtered (no motion artifacts)
      % rectified
      % envelopes
   end

end


function feat = compFeat_Kinematic(footOff, footStrike, hip, knee, ankle, MTP, varargin)

   % TODO: instead of providing each joint as an input parameter, define
   % dynamic list of joints. Features wpi;d be computed only for the joint
   % given as input. Different joints may have different features. 

   nStride = length(footStrike);
   
   % The first optional argument is the limb vector angle. If it is
   % provided, kinematic features will be computed for that too.
   
   % The vector idxGoog is a logical array with 0 on the i-th element if
   % the i-th stride has to be discarded, and 1 otherwise.

   nvrgs            = length(varargin);
   optargs          = {[],ones(nStride,1)};   % default values
   optargs(1:nvrgs) = varargin;               % overwrite with inputs

   [limbV, idxGood] = optargs{:};

   

   %%%%%%%%%%%%%%%%%%%%%%%%% Hip %%%%%%%%%%%%%%%%%%%%%%%%%

   peak_flexion_hip   = NaN;   % Peak of hip flexion
   peak_extension_hip = NaN;   % Peak of hip extension
   range_flexion_hip  = NaN;   % Hip range of motion

   k=1;
   for i=1:nStride-1   
      if idxGood(i)

         peak_flexion_hip(k,1)   = max(hip(footStrike(i):footStrike(i+1)));     % IMPORTANT: this assumes to use hip bottom marker
         peak_extension_hip(k,1) = min(hip(footStrike(i):footStrike(i+1)));
         range_flexion_hip(k,1)  = peak_flexion_hip(k) - peak_extension_hip(k);
         
         k=k+1;
         
      end
   end

   feat.hip_peakFlex = peak_flexion_hip(:);
   feat.hip_peakExt  = peak_extension_hip(:);
   feat.hip_range    = range_flexion_hip(:);


   %%%%%%%%%%%%%%%%%%%%%%%%% Knee %%%%%%%%%%%%%%%%%%%%%%%%%

   flexion_initial_contact_knee = NaN;  % Knee flexion at initial contact
   time_peak_flexion_knee       = NaN;  % Time of peak knee flexion [%Gait]
   peak_flexion_knee            = NaN;  % Peak knee flexion
   time_peak_extension_knee     = NaN;  % Time of peak knee extension [%Gait]
   peak_extension_knee          = NaN;  % Peak knee extension
   range_flexion_knee           = NaN;  % Range of knee flexion

   k=1;
   for i=1:nStride-1   
      if idxGood(i)

         flexion_initial_contact_knee(k,1) = knee(footStrike(i));                           
         [num, idx]                        = min(knee(footStrike(i):footStrike(i+1)));
         time_peak_flexion_knee(k,1)       = idx/(footStrike(i+1)-footStrike(i))*100;
         peak_flexion_knee(k,1)            = num;                                           
         [num, idx]                        = max(knee(footStrike(i):footStrike(i+1)));      
         time_peak_extension_knee(k,1)     = idx/(footStrike(i+1)-footStrike(i))*100;       
         peak_extension_knee(k,1)          = num;                                           
         range_flexion_knee(k,1)           = peak_extension_knee(k) - peak_flexion_knee(k); 

         k=k+1;
         
      end   
   end

   feat.knee_flexInCont    = flexion_initial_contact_knee(:);
   feat.knee_peakFlexTPerc = time_peak_flexion_knee(:);
   feat.knee_peakFlex      = peak_flexion_knee(:);
   feat.knee_peakExtTPerc  = time_peak_extension_knee(:);
   feat.knee_peakExt       = peak_extension_knee(:);
   feat.knee_range         = range_flexion_knee(:);


   %%%%%%%%%%%%%%%%%%%%%%%%% Ankle %%%%%%%%%%%%%%%%%%%%%%%%%

   peak_dorsiflexion_stance_ankle = NaN; % Peak ankle dorsiflexion in stance
   peak_dorsiflexion_swing_ankle  = NaN; % Peak ankle dorsiflexion in swing
   peak_dorsiflexion_ankle        = NaN; % Peak ankle dorsiflexion
   peak_plantarflexion_ankle      = NaN; % Peak ankle plantarflexion
   range_flexion_ankle            = NaN; % Range of ankle flexion

   k=1;
   for i=1:nStride-1   
      if idxGood(i)

         % First footOff after the ith footStrike
         idx_fOff                            = find(footOff>footStrike(i));
         peak_dorsiflexion_stance_ankle(k,1) = min(ankle(footStrike(i):footOff(idx_fOff(1))));
         peak_dorsiflexion_swing_ankle(k,1)  = min(ankle(footOff(idx_fOff(1)):footStrike(i+1)));
         peak_dorsiflexion_ankle(k,1)        = min(ankle(footStrike(i):footStrike(i+1)));      
         peak_plantarflexion_ankle(k,1)      = max(ankle(footStrike(i):footStrike(i+1)));
         range_flexion_ankle(k,1)            = peak_plantarflexion_ankle(k) - peak_dorsiflexion_ankle(k);

         k=k+1;
         
      end   
   end

   feat.ankle_peakDorsiFlex_St = peak_dorsiflexion_stance_ankle(:);
   feat.ankle_peakDorsiFlex_Sw = peak_dorsiflexion_swing_ankle(:);
   feat.ankle_peakDorsiFlex    = peak_dorsiflexion_ankle(:);
   feat.ankle_peakPlantFlex    = peak_plantarflexion_ankle(:);
   feat.ankle_range            = range_flexion_ankle(:);


   %%%%%%%%%%%%%%%%%%%%%%%%% MTP %%%%%%%%%%%%%%%%%%%%%%%%%

   peak_flexion_MTP   = NaN;
   peak_extension_MTP = NaN;
   range_flexion_MTP  = NaN;

   k=1;
   for i=1:nStride-1  
      if idxGood(i)

         peak_flexion_MTP(k,1)   = min(MTP(footStrike(i):footStrike(i+1)));
         peak_extension_MTP(k,1) = max(MTP(footStrike(i):footStrike(i+1)));       
         range_flexion_MTP(k,1)  = peak_extension_MTP(k) - peak_flexion_MTP(k);
         
         k=k+1;

      end   
   end

   feat.mtp_peakFlex = peak_flexion_MTP(:);
   feat.mtp_peakExt  = peak_extension_MTP(:);
   feat.mtp_range    = range_flexion_MTP(:);


   %%%%%%%%%%%%%%%%%%%%%%%%% limb Vector %%%%%%%%%%%%%%%%%%%%%%%%%%%

   if ~isempty(limbV)

      peak_flexion_limbV   = NaN;
      peak_extension_limbV = NaN;
      range_flexion_limbV  = NaN;

      k=1;
      for i=1:nStride-1
         if idxGood(i)

            peak_flexion_limbV(k,1)   = min(limbV(footStrike(i):footStrike(i+1)));         
            peak_extension_limbV(k,1) = max(limbV(footStrike(i):footStrike(i+1)));         
            range_flexion_limbV(k,1)  = peak_extension_limbV(k) - peak_flexion_limbV(k);

            k=k+1;
            
         end
      end

      feat.limbV_ang_peakFlex = peak_flexion_limbV(:);
      feat.limbV_ang_peakExt  = peak_extension_limbV(:);
      feat.limbV_ang_range    = range_flexion_limbV(:);

   end
   
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Possible additional features (to be adapted) and Filipe's previous code


%%%%%%%%%%%%%%%%%%% HIP %%%%%%%%%%%%%%%%%%%
% Pre-swing hip angle
% Peak of hip abduction in swing
% Range of hip abduction
% Mean hip rotation in stance (ROM)

%     range_abduction_hip_Left(i) = max(angle_hip_interpolated(Left_FS(i):Left_FS(i+1),2) - min(angle_hip_interpolated(Left_FS(i):Left_FS(i+1),2)));
    
%     % First Left FO after the ith Left FS
%     ind_Left_FO = find(Left_FO>Left_FS(i));
%     % First Right FS after the ith Left FS
%     ind_Right_FS = find(Right_FS>Left_FS(i));
%     pre_swing_angle_hip(i) = min(angle_hip_interpolated(Right_FS(ind_Right_FS(1)):Left_FO(ind_Left_FO(1))));
%     peak_abduction_swing_hip_Left(i) = max(angle_hip_interpolated(Left_FO(ind_Left_FO(1)):Left_FS(i+1),2));
%     mean_rotation_stance_hip_Left(i) = max(angle_hip_interpolated(Left_FS(i):Left_FO(ind_Left_FO(1)),3)) - min(angle_hip_interpolated(Left_FS(i):Left_FO(ind_Left_FO(1)),3));


% %%%%%%%%%%%%%%%%%%% Kinematics data %%%%%%%%%%%%%%%%%%%
% % Low pass filtered - 4th orther Butterworth
% LP = 20;
% fNorm = LP/(sampling_frequency/2);
% [b,a] = butter(4, fNorm, 'low');
% 
% [n_rows, n_columns] = size(Kinematics);
% 
% for i = 1:n_columns % number of variables
%     Filtered_Kinematics(1:n_rows, i) = filtfilt(b, a, Kinematics(1:n_rows, i));
% end
% 
% Kinematics = Filtered_Kinematics;
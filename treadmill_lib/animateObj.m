%% AMINATEOBJ projects a set of vectors onto a plane
%
%animateObj( data )
%
%INPUTS:
%
%data: 1-by-M cell array containing the coordinates of M markers. Each
%element of the cell is a N-by-2 (or N-by-3) matrix containing N samples of
%the XY (XYZ) coordinates.
%
%freq: frequency of rendering in Hz
%
%Author: Cristiano Alessandro (cristiano.alessandro@northwestern.edu)
%Date: April 06 2016
%Licence: GNU GPL

%% Copyright (c) 2016 Cristiano Alessandro <cristiano.alessandro@northwestern.edu>
%
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program. If not, see <http://www.gnu.org/licenses/>.
%

function animateObj( data, freq )    

    nDim = size(data{1});
    
    X = cell2mat(cellfun(@(x)x(:,1),data,'UniformOutput', false));
    Y = cell2mat(cellfun(@(x)x(:,2),data,'UniformOutput', false));
    
    if nDim>2
        Z = cell2mat(cellfun(@(x)x(:,3),data,'UniformOutput', false));
        h = line(X(1,:),Y(1,:),Z(1,:),'Marker','o');
    else
        h = line(X(1,:),Y(1,:),'Marker','o');
    end    
    
    nPt = size(X,1);
    for i=2:nPt
        if nDim>2
            set(h,'XData',X(i,:),'YData',Y(i,:),'ZData',Z(i,:));
        else
            set(h,'XData',X(i,:),'YData',Y(i,:));
        end
        %keyboard();
        pause(1/freq)
    end

end


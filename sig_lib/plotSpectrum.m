% Code based on:
% http://www.staff.vu.edu.au/msek/frequency%20analysis%20-%20fft.pdf
% http://math.stackexchange.com/questions/636847/understanding-fourier-transform-example-in-matlab
% http://www.mathworks.com/matlabcentral/answers/15770-scaling-the-fft-and-the-ifft
% https://en.wikipedia.org/wiki/Parseval%27s_theorem

function plotSpectrum ( dat, fs )


   l        = length(dat); 		% Length of data
   NFFT     = 2^nextpow2(l);     % Next power of 2 from length of y      
   idxNyq   = NFFT/2 + 1;			% index of the Nyquist frequency


   y      	= fft(dat,NFFT);   	% Discrete FFT of the signal 

   
   % The result is scaled by fs to make sure that Parseval's theorem holds
   % (i.e. the energy of the signal should be equal to the energy of the FFT).
   % This is unimportant if only the qualitative shape of the FFT matters.
   y        = y/fs;              
   
   % Magnitude of the FFT
   yM       = abs(y);		
   
   % The second part of the FFT contains the negative frequency information.
   % This is because the spectrum of a dicrete signal is repeted every 2*pi*fs.
   % In other words, we convert a two-sided spectrum to a one-sided spectrum.
   yMTr     = yM(1:idxNyq);	
   
   % Compensate for truncating the negative frequencies. Half of the energy is lost,
   % therefre the FFT should be multiplied by 2. This is importnat to make sure that
   % Parseval's theorem holds.
   yMTr(2:end-1) = 2*yMTr(2:end-1);	

   % Frequency vector: from zero to Nyquist frequency.
   f = fs/2 * linspace(0,1,idxNyq);
   
   plot(f,yMTr)
   title('Single-Sided Amplitude Spectrum')
   xlabel('Frequency [Hz]')
   ylabel('|Y(f)|')
   
end


load ('./aft_s10_d30_01.mat');

% I do not provide normal vectors, thus angle are in the range [0;180]
ang_0pi = computeAngle(rat_sg.heel,rat_sg.metatarsal,rat_sg.phalanx);

% Normal
n = repmat([0 0 1],length(rat_sg.heel),1);

% I provide normal vectors n, thus angle are in the range [0;360]
ang_02pi = computeAngle(rat_sg.heel,rat_sg.metatarsal,rat_sg.phalanx,n);
         
figure         
plot(ang_0pi)
hold on
plot(ang_02pi,'r')
axis tight
legend('[0;180]','[0;360]')
grid on
function [ idxGood, footStrike, footOff ] = checkGaitSegmentation(footStrike, footOff, data, freq)
   
   lFS = length(footOff);
   lFO = length(footStrike);
   
   if lFS~=lFO
      error('# foot-srikes different than # foot-offs');
   end
   
   nDim = size(data{1});
   
   fig                  = gcf;   
   fig.UserData.idxGood = [];
   fig.UserData.str     = 'Enter to accept, or ''d'' to discard.';
   
   % Cycle over strides
   for j=1:lFS
      
      % Extract single stride
      dataTmp  = cellfun(@(x)x(footStrike(j):footOff(j),:), data, ...
                        'UniformOutput',false);
        
      % Animate
      [~,~,ha] = segmentGaitAnimation( dataTmp, freq, 'animate');
      
      % Text box for messages
      ha.footStrikeString.String = ['Stride ' num2str(j)];
      set(ha.footOffString,'ForegroundColor','black');
      ha.footOffString.String = fig.UserData.str;
      
      % Set Callback (it has to be done here to overwrite the callback
      % defined inside segmentGaitAnimation)
      fig.KeyPressFcn = @segChk_figCallBack;
      
      % Block until correct button is pressed
      uiwait(fig)
      
      % Cancel animated object
      if nDim>2
         set(ha.h,'XData',[],'YData',[],'ZData',[]);
      else
         set(ha.h,'XData',[],'YData',[]);
      end
      
      % Cancel text
      ha.footStrikeString.String = '';
      ha.footOffString.String    = '';
      ha.textFrame.String        = '';
      ha.textTime.String         = '';
      
   end  
   
   idxGood = fig.UserData.idxGood;

end


function segChk_figCallBack(hObject,callbackdata)

    data = hObject.UserData;    
    key  = callbackdata.Key;
    
    str  = data.str;
    
    obj  = hObject.Children.findobj('type','UIControl');
    idx  = arrayfun(@(x)strcmpi(x.String,str), obj);
    obj  = obj(idx);
    
    switch key           
        case{'return'}
            
            data.idxGood = [data.idxGood; 1];
            obj.String   = [str ' ACCEPTED'];
            
            pause(0.5);
            
            obj.String       = str;
            hObject.UserData = data;
            
            uiresume(hObject);
            
        case{'d'}
           
            data.idxGood = [data.idxGood; 0];
            obj.String   = [str ' DISCARDED'];
            
            pause(0.5);
            
            obj.String       = str;
            hObject.UserData = data;
            
            uiresume(hObject);
       otherwise
            % Wrong button
            obj.String = [str ' WRONG BUTTON'];
            pause(0.5);
            obj.String = str;
    end        

end
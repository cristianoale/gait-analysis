load ./demo/aft_s10_d30_01.mat

%%

rat_tm_yz = {rat_tm.hip_top(:,[2,3])    ...
             rat_tm.hip_bottom(:,[2,3]) ...
             rat_tm.hip_center(:,[2,3]) ...
             rat_tm.knee(:,[2,3])       ...
             rat_tm.heel(:,[2,3])       ...
             rat_tm.metatarsal(:,[2,3]) ...
             rat_tm.phalanx(:,[2,3]) };
    

tdm_tm_yz = tdm_tm(:,[2,3]);

xMin = cell2mat(cellfun(@(x)min(x(:,1)),rat_tm_yz,'UniformOutput', false));
xMin = min([xMin min(tdm_tm_yz(:,1))]);

xMax = cell2mat(cellfun(@(x)max(x(:,1)),rat_tm_yz,'UniformOutput', false));
xMax = max([xMax max(tdm_tm_yz(:,1))]);

yMin = cell2mat(cellfun(@(x)min(x(:,2)),rat_tm_yz,'UniformOutput', false));
yMin = min([yMin min(tdm_tm_yz(:,2))]);

yMax = cell2mat(cellfun(@(x)max(x(:,2)),rat_tm_yz,'UniformOutput', false));
yMax = max([yMax max(tdm_tm_yz(:,2))]);

figure
plot(tdm_tm_yz(:,1),tdm_tm_yz(:,2),'o')
axis([xMin xMax yMin yMax])
grid on
hold on

segmentGaitAnimation( rat_tm_yz , 100);

%% Automatic segmentation

% Signal
foot_z = [rat_tm.heel(:,3), rat_tm.metatarsal(:,3), rat_tm.phalanx(:,3)];

figure

plot(foot_z)
axis tight 
hold on
legend('heel','toe','phalanx')

plot(repmat(fStrike,1,3),foot_z(fStrike,:),'o')     % Foot strike
plot(repmat(fOff,1,3),foot_z(fOff,:),'x')           % Foot off

% Derivative
fzd = diff(foot_z);

figure
plot(fzd)
axis tight 
hold on
legend('heel','toe','phalanx')

plot(repmat(fStrike,1,3),fzd(fStrike,:),'o')     % Foot strike
plot(repmat(fOff,1,3),fzd(fOff,:),'x')           % Foot off

% Second derivative
fzdd = diff(fzd);

figure
plot(fzdd)
axis tight 
hold on
legend('heel','toe','phalanx')

plot(repmat(fStrike,1,3),fzdd(fStrike,:),'o')     % Foot strike
plot(repmat(fOff,1,3),fzdd(fOff,:),'x')           % Foot off


%% Interpolating and filtering

% Check for NaN and interpolate
for j=1:size(fzd,2)
    nanx          = isnan(fzd(:,j));
    t             = 1:numel(fzd(:,j));
    fzd (nanx,j)  = interp1(t(~nanx), fzd(~nanx,j), t(nanx), ...
        'linear','extrap');
end

figure, plot(fzd)
hold on

% Filter derivative
fzd_f = filterData(fzd, 15, rat.f, 3);
plot(fzd_f,'linewidth',2)

plot(repmat(fStrike,1,3),fzd_f(fStrike,:),'o')     % Foot strike
plot(repmat(fOff,1,3),fzd_f(fOff,:),'x')           % Foot off



%% Algorithm

clear idxMax idxMin

dat = fzd_f;
%dat = fzd;

tshMax  = [10 10 7];
tshMin  = [3 3 3];
epsD = 10;
epsZ = 2;

%for j=1:size(dat,2)
j=2;
    
    idxAll     = detectPeaks ( dat(:,j), epsD );         % All the peaks
    tmp        = dat(idxAll,j)>tshMax(j) ;      
    idxMax{j}  = idxAll(tmp);                           % Foot-off
    tmp        = find( dat(idxAll,j)<-tshMax(j) );
    idxMin{j}  = idxAll(tmp);                           
    
    tmp_iMax   = idxMax{j};
    tmp_iMin   = idxMin{j};
    
    %keyboard();
    
    for k=1:length(tmp_iMax)-1  
       
        %plot(tmp_iMax(k),dat(tmp_iMax(k),j),'ro')
        
        tmp = tmp_iMin>tmp_iMax(k) ;
        tmp = tmp_iMin(tmp);
        
        if ~isempty(tmp)
           
            %plot(tmp(1),dat(tmp(1),j),'go')
            
            idxSt = tmp(1);                         % Index of minimum peak
            idxEd = tmp_iMax(k+1);
            tmp   = find( dat(idxSt:idxEd,j)>-epsZ );
            
            if ~isempty(tmp)
               idxZero(k,1) = idxSt + tmp(1)-1;    % Foot strike  
               %plot(idxZero(k,1),dat(idxZero(k,1),j),'rx')
            else
               fprintf('No zero\n');
            end
        else
           fprintf('No minimum\n');
        end                
        
    end
        
    idxZero(idxZero==0) = [];
    
    idxMin{j} = idxZero(:,1);
            
%end


%% Plotting

mk = 2;

clf
plot(dat(:,mk))
hold on
plot(idxMax{mk},dat(idxMax{mk},mk),'ro')
plot(idxMin{mk},dat(idxMin{mk},mk),'rx')


%% Formatting events

mk      = 2;            % Toe
fStrike = idxMin{mk};
fOff    = idxMax{mk};

% The first event should be a foot strike
while fStrike(1)>fOff(1)
   fOff = fOff(2:end);
end

% The last event should be a foot off
while fStrike(end)>fOff(end)
   fStrike = fStrike(1:end-1);
end

% There should be the same number of foot strikes and foot offs
if length(fOff)~=length(fStrike)
   error('Some events have not been detected');
end


%%

figure

mk = 2;

clf
plot(dat(:,mk))
hold on
plot(idxMax{mk},dat(idxMax{mk},mk),'ro')
plot(idxMin{mk},dat(idxMin{mk},mk),'rx')

plot(fOff,dat(fOff,mk),'gx')
plot(fStrike,dat(fStrike,mk),'go')

plot(fzd(:,mk),'m')

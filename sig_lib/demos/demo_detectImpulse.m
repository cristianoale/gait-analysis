close all
clear all

load ./data/VL_above_v80_4.mat

sig = data(:,4);
tsh = 0.02;

idx = detectImpulse(sig, tsh);

plot(sig)
hold on
plot(idx,sig(idx),'or')
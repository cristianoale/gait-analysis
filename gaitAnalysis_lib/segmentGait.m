function [footStrike, footOff] = segmentGait( marker, filt, fq, tsh, epsDer, epsZer, floorZ, varargin )

   if tsh(1)>tsh(2)
      error('minimum treshold should be the first element of parameter tsh');
   end
   
   if nargin>6
      flagPlot = varargin{1};
   else
      flagPlot = false;
   end
   
   filtOrd = filt(1);      % Filter order
   filtCOf = filt(2);      % Filter cut-off frequency
   
   sampRt  = fq;           % sampling rate of the data
   
   tshMax  = tsh(2);       % threshold to find max peaks of derivative
   tshMin  = tsh(1);       % threshold to find min peaks of derivative
   epsD    = epsDer;       % tolerance to find peaks of derivative (i.e. zeros of second derivative)
   epsZ    = epsZer;       % tolerance to find zeros of derivative
   
   posFloor = floorZ(1);   % z-coordinate of the floor of the tradmill
   espFloor = floorZ(2);   % tolerance, floorZ(1)+-floor(2)
   

   % Derivative of the z-coordinate (ie. vertical axis)
   fzd = diff(marker(:,3));
   
   % Check for NaN and interpolate   
   nanx       = isnan(fzd);
   t          = 1:numel(fzd);
   fzd (nanx) = interp1(t(~nanx), fzd(~nanx), t(nanx), 'linear','extrap');
   
   % Filter derivative
   fzd_f = filterData(fzd, filtCOf, sampRt, filtOrd);
   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   idxAll  = detectPeaks ( fzd_f, epsD );       % All the peaks
   tmp     = fzd_f(idxAll)>tshMax;
   idxMax  = idxAll(tmp);                       % Foot-off: the derivative of the position of the foot (wrt time) is maximal
   tmp     = fzd_f(idxAll)<tshMin ;   
   idxMin  = idxAll(tmp);   

   for k=1:length(idxMax)-1
      
      tmp = idxMin>idxMax(k) & idxMin<idxMax(k+1);
      tmp = idxMin(tmp);
      
      if ~isempty(tmp)
         
         idxSt = tmp(1);                         % Index of minimum peak
         idxEd = idxMax(k+1);
         
         % Using the raw data
         tmp   = find( fzd(idxSt:idxEd)>-epsZ & ...                     % Foot-strike is an event in which both the derivative (wrt time)
                       abs(marker(idxSt:idxEd,3)-posFloor)<espFloor);   % and the position (z-coordinate) are zero. The derivative because 
                                                                        % during the stance phase the height of the foot does not change, the
                                                                        % position because the foot is at its lowest height (on the floor).
         
         % Using the filtered data            
%          tmp   = find( fzd_f(idxSt:idxEd)>-epsZ & ...                   % Foot-strike is an event in which both the derivative (wrt time)
%                        abs(marker(idxSt:idxEd,3)-posFloor)<espFloor);   % and the position (z-coordinate) are zero. The derivative because 
                                                                          % during the stance phase the height of the foot does not change, the
                                                                          % position because the foot is at its lowest height (on the floor). 
         if ~isempty(tmp)                                                 
            idxZero(k,1) = idxSt + tmp(1)-1;     % Foot strike
         else                        
            fprintf('No zero, stride #%d, index %d\n',k,idxMax(k));
         end
      else         
         fprintf('No minimum, stride #%d, index %d\n',k,idxMax(k));
      end
      
   end      
   
   
   %%%%%%%%%%%%%%%%% Format events %%%%%%%%%%%%%%%%%
   
   footStrike = idxZero;
   footOff    = idxMax;
   
   idxNull             = find(footStrike==0);
   footStrike(idxNull) = [];
   footOff(idxNull)    = [];
   
   % The first event should be a foot strike
   while footStrike(1)>footOff(1)
      footOff = footOff(2:end);
   end
   
   % The last event should be a foot off
   while footStrike(end)>footOff(end)
      footStrike = footStrike(1:end-1);
   end
   
   while length(footOff)>length(footStrike) && ...
         footOff(end)-footStrike(end)>0 &&     ...
         footOff(end-1)-footStrike(end)>0 &&   ...
         footOff(end)-footStrike(end)>footOff(end-1)-footStrike(end)
      
      footOff = footOff(1:end-1);
   end
   
   % There should be the same number of foot strikes and foot offs
   if length(footOff)~=length(footStrike)
      fprintf('WARNING: Some events may not have been detected\n');
   end      
   
      
   if flagPlot
      
      tSpan = linspace( 0,length(marker(:,3))/sampRt,length(marker(:,3)) );
      
      figure
      
      subplot(2,1,1)
      plot(tSpan,marker(:,3))
      axis tight, hold on
      plot(tSpan(footOff),marker(footOff,3),'rx')
      plot(tSpan(footStrike),marker(footStrike,3),'ro')
      grid on
      ylabel('pos-z')
      legend('raw','foot off','foot strike');
      
      subplot(2,1,2)
      plot(tSpan(1:end-1),fzd)
      axis tight, hold on
      plot(tSpan(1:end-1),fzd_f)
      plot(tSpan(footOff),fzd_f(footOff),'rx')
      plot(tSpan(footStrike),fzd_f(footStrike),'ro')
      grid on
      ylabel('derivative-z')
      legend('raw','filtered','foot off','foot strike');
      
   end

end


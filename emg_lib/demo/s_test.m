%% data

%dat = csvread('./data_test/test107.csv',5,2);
dat = load('./data_test/emg.mat');
dat = dat.emg_dat;

fs  = 5000;      % Sampling frequency [Hz]
t   = linspace(0,length(dat)/fs,length(dat))';


%% setting filters

fOrd_ma = 3;            % filter order (motion artifacts)
band_ma = [20; 500];    % filter bandpass [Hz]

fOrd_e  = 3;            % filter order (envelopes)
fc_e    = 20;           % filter lowpass [Hz]


%% process data

[ emg_f, emg_r, emg_e ] = processEMG (dat , fs, fOrd_ma, band_ma, ...
                                                fOrd_e,  fc_e);
  
                                             
%% plotting

trace = 7;

figure

% raw
subplot(3,1,1)
plot(t,dat(:,trace))
axis tight
ylabel('raw')
grid on

% filtered
subplot(3,1,2)
plot(t,emg_f(:,trace))
axis tight
ylabel('filtered')
grid on

% rectify and envelope
subplot(3,1,3)
plot(t,emg_r(:,trace))
hold on
plot(t,emg_e(:,trace),'r','linewidth',2)
axis tight
xlabel('time [s]')
ylabel('rectified and envelope')
legend('rectified','envelope')
grid on


figure
%plotSpectrum(emg_f),fs)
plotSpectrum(dat(:,trace),fs)
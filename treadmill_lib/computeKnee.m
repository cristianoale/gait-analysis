% Code base on: http://mathworld.wolfram.com/Circle-CircleIntersection.html

function knee = computeKnee ( hip, kneeMk, heel, femurL, tibiaL )   

    lHip = length(hip);
    lKn  = length(kneeMk);
    lHl  = length(heel);

    if lHip~=lKn && lKn~=lHl
        error('hip, knee and heel vectors must have the same length');
    end

    % There should be no NaN. Interpolate!
    nanx       = isnan(hip);
    t          = 1:numel(hip);
    if any(~nanx)
      hip (nanx) = interp1(t(~nanx), hip(~nanx), t(nanx), 'linear','extrap');
    end

    nanx          = isnan(kneeMk);
    t             = 1:numel(kneeMk);
    if any(~nanx)
      kneeMk (nanx) = interp1(t(~nanx), kneeMk(~nanx), t(nanx), 'linear','extrap');
    end

    nanx        = isnan(heel);
    t           = 1:numel(heel);
    if any(~nanx)
      heel (nanx) = interp1(t(~nanx), heel(~nanx), t(nanx), 'linear','extrap');
    end

    knee = NaN(lHip,3);

    if all(~isnan(hip(:))) && all(~isnan(kneeMk(:))) && all(~isnan(heel(:)))
    
       for j=1:lHip

           dat = [hip(j,:) ; kneeMk(j,:); heel(j,:)];

           d       = norm(heel(j,:)-hip(j,:));   % distance beetween heel and hip
           [n,~,~] = fitPlane(dat);    % Fit plane to hip, knee and heel

           % Computation of a frame of reference centered in
           % the hip, with the x-axis pointing towards the heel, and the z-axis
           % pointing towards the animal. This choice ensures that the y-coordinate
           % of the knee is the positive solution of the equation used for knY_hip
           o   = hip(j,:);
           x   = ( heel(j,:)-hip(j,:) ) ./ d;
           z   = n;
           y   = cross(z,x);

           % To make sure that the z-axis points towards the animal, I construct a
           % vector from the hip to the knee, and I project it onto the y-axis.
           % If this projection is negative, then the z-axis is pointing to the
           % wrong direction. 
           % IMPORTANT: THISS ASSUMES THAT THE MARKER OF THE KNEE IS ALWAYS ON THE
           % SAME SIDE OF AXIS X (THIS SHOULD BE OK, BUT SOME ISSUES MAY OCCUR
           % BECAUSE OF THE ELASTICITY OF THE SKIN, ON WHICH THE KNEE MARKER IS
           % ATTACHED).
           vTmp = kneeMk(j,:)-heel(j,:);
           VPj  = vTmp*y';
           if VPj<0
               z =-z;
               y = cross(z,x);
           end

           R_g2h = [x' y' z']; % Rotation of the hip coordinate system wrt to 
                               % global. Multiplying by a point expressed in the   
                               % hip FoR, one obtains the global coordinate of the 
                               % point.

           RT_g2h = [R_g2h      o'; ...     % Rototransl. hip FoR wrt global
                     zeros(1,3) 1];                   

           % Computation of the knee position wrt the constructed FoR
           term    = d^2 - tibiaL.^2 + femurL.^2;
           knX_hip = term / (2*d);
           knY_hip = sqrt( (4*d^2*femurL^2 - term^2) / (4*d^2) );
           knZ_hip = 0;
           kn_hip  = [knX_hip; knY_hip; knZ_hip];

           % Knee position in the global coordinate system
           knee_tmp  = RT_g2h * [kn_hip;1];
           knee(j,:) = knee_tmp(1:3);

       end

       % If some elements are complex numbers, get rid of them. This can
       % happen when markers disappear, thus the corresponding element is NaN
       knee( logical(imag(knee)) ) = NaN;
    
    end

end


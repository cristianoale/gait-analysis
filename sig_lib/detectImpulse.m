%% DETECTIMPULSE detects the indexes where peaks start
%
% idx = detectImpulse( sig, tsh ) 
%
% This function assumes that the input signal sig is flat with a few
% imulses. It resturs the vector indexes corresponding to the onsets of 
% these impulses (NB. the indexes do NOT correspond to the maximum values 
% of the impulses, but to their onsets).
%
% INPUT:
% sig: n-by-1 vector containing the signal with the impulses
% tsh: scalar value used as a threshold on the derivative of the signal sig
% to detect the impulses.
%
% OUTPUT:
% idx: m-by-1 vector with the indexes corresponding to m impulse onsets. 
%
%Author: Cristiano Alessandro (cristiano.alessandro@northwestern.edu)
%Date: September 15 2016
%Licence: GNU GPL

function idx = detectImpulse( sig, tsh )

   % Derivative (not really!) of signal
   dSig  = diff(sig);
   dSig  = [dSig; dSig(end)];

   % Find high derivative indexes
   idxD  = find(dSig>tsh);

   % Diff of the indexes
   dIdxD = diff(idxD);

   % There can be adjacent indexes corresponding to derivative higher than
   % threshold (corresponding to the rising of the peaks). We are only interested in
   % the first indexes. Thus we eliminate the indexes next to those whose
   % derivative is 1. If there are no adjacent ones, that means that the
   % algorithm has already identified the beginning of the peaks.

   if any(dIdxD==1)   
      i = find(dIdxD==1);
      idxD(i+1) = [];     
   end

   idx = idxD;

end


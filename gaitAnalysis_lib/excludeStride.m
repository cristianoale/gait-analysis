function [ idxGood, footStrike, footOff ] = excludeStride ( fStrike, fOff, maxStanceDur, freq )
   
   % For the moment, no additional inclusion criteria is implemented. This
   % is the code that can be used to add variable arguments to the function.
    
   % nvrgs            = length(varargin);
   % optargs          = {[] []};          % default values
   % optargs(1:nvrgs) = varargin;         % overwrite with inputs
 
   % [marker, minPosTread] = optargs{:};
   
   
   %%%%%%%%%%%%%%%%%%%%%%%% Check data structures %%%%%%%%%%%%%%%%%%%%%%%%
      
   % First foot off after first foot strike
   ind_FO = find(fOff>fStrike(1));

   if ~ind_FO(1)==1
      error('First foot off is not after first foot strike.');
      %fOff = fOff(ind_FO);      
      %fprintf('WARNING: first foot off is not after first foot strike. ');
      %fprintf('I ignore first foot offs. Check your data!\n');
   end

   % To make sure that # Foot Strike events is equal to # Foot Off events 
   lFO = length(fOff);
   lFS = length(fStrike);
   
   if lFO>lFS
      error('# foot offs > # foot strikes.')
      %fOff = fOff(1:lFS);
      %fprintf('WARNING: # foot offs > # foot strikes. ');
      %fprintf('I ignore last foot offs. Check your data!\n');
   end
   
   if lFO<lFS
      fprintf('# foot strikes > # foot offs');
      %fStrike = fStrike(1:lFO);
      %fprintf('WARNING: # foot strikes > # foot offs. ');
      %fprintf('I ignore last foot strikes. Check your data!\n');
   end

   % At this point, # foot strikes = # foot offs
   
   
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   stcDurIdx  = fOff-fStrike;    % Foot strikes should be consecutive
   stcDur     = stcDurIdx/freq;  % Stride duration
    
   idxGood    = stcDur<maxStanceDur;   % Only strides with short stance
   footOff    = fOff(idxGood);
   footStrike = fStrike(idxGood);
   
end
load('./data/data_test.mat')

%%% Blood volume %%%

% Mild filter
%fOrd_BV = 5;    % filter order for Blood Volume sensors
%fc_BV   = 0.1;  % cut-off frequency of Blood Volume sensors [Hz]

% Very aggressive filter (Blood volume)
fOrd_BV = 3;    % filter order for Blood Volume sensors
fc_BV   = 0.05; % cut-off frequency of Blood Volume sensors [Hz]

% Sampling frequency
fs = 1/(t(2)-t(1));

data_filt = filterData(dat, fc_BV, fs, fOrd_BV);

%Plotting
figure();
plot(t,dat)
hold on
plot(t,data_filt,'linewidth',2)
axis tight
xlabel('Time [s]'), ylabel('NIRS')
legend('Raw','Filtered')

% Demo to use the geometry functions for the treadmill

clear all
close all

addpath('../geometry_lib')

%% Load data

path  = '../data/an200_sp10_du1_01.csv';
l_dat = 5;  % Line in which the useful data begin

%Loading the data
dat = csvread(path,l_dat);

%Treadmill markers
tdm_dat = dat(:,21:end);

%Mean (because the tradmill markers do not move during the experiment)
tdm_dat_m = mean(tdm_dat);

%Reshape to obtain a #markers-by-3 matrix (columns are x,y,z coordinates)
tdm       = (reshape(tdm_dat_m,3,4))'; 

%% Actual computations

% Fit sagittal plane n(1)*x+n(2)*y+n(3)*z=d
[n,d,p] = fitPlane (tdm);

% Compute:
% (1) RT_g2t: Rototranslation of the treadmill FoR wrt to global frame
% (2) RT_t2g: Rototranslation of the global FoR wrt to the treadmill FoR
[RT_g2t, RT_t2g] = computeTreadmillFoR (tdm_dat);

% Treadmill Frame of Reference
o = RT_g2t(1:3,4); % origin: last column of the matrix RT_g2t
x = RT_g2t(1:3,1);
y = RT_g2t(1:3,2);
z = RT_g2t(1:3,3);

% To verify the axes are all orthogonal
xyz = RT_g2t(1:3,1:3)*RT_g2t(1:3,1:3)'

%% Plotting

% Plotting sagittal plane
clf
plot3(tdm(:,1),tdm(:,2),tdm(:,3),'o')
hold on
plot3(p(:,1),p(:,2),p(:,3),'c*')
quiver3(p(1),p(2),p(3),n(1),n(2),n(3),'r','linewidth',2)
[X,Y] = meshgrid(linspace(-600,0,3));
surf(X,Y, - (n(1)/n(3)*X+n(2)/n(3)*Y-dot(n,p)/n(3)),'facecolor','red','facealpha',0.5);

% Plotting new FoR
quiver3(o(1),o(2),o(3),x(1)*100,x(2)*100,x(3)*100,'k','linewidth',5)
quiver3(o(1),o(2),o(3),y(1)*100,y(2)*100,y(3)*100,'g','linewidth',5)
quiver3(o(1),o(2),o(3),z(1)*100,z(2)*100,z(3)*100,'c','linewidth',5)

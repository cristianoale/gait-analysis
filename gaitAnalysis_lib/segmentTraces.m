function dat_i = segmentTraces( fStrike, data , varargin )

    nSamp   = 100;
    nStride = length(fStrike);    
    
    % If falg is true, the function fills the NaN with a linear
    % interpolation (and extrapolates if NaN are at the beginning or at the
    % end of the vector data). The default is flag = false.
    
    % the vector idxGoog is a logical array with 0 on the i-th element if 
    % the i-th stride has to be discarded, and 1 otherwise.
    
    nvrgs            = length(varargin);
    optargs          = {false, ones(nStride,1), 1};   % default values
    optargs(1:nvrgs) = varargin;                      % overwrite with inputs
    
    [flag, idxGood, fracNaN]  = optargs{:};
        
    %nGdStride = sum(idxGood==1);        
    dat_i     = NaN(nSamp,nStride-1);        
        
    % There will be NaN column where idxGood=0
    for j=1:nStride-1
       
        if idxGood(j)
       
           dat_s      = data(fStrike(j):fStrike(j+1)-1,1);  % This assumes a one-dimensional vector
           len_s      = length(dat_s);
           tOld       = linspace(0,1,len_s);
           tNew       = linspace(0,1,nSamp);
           dat_i(:,j) = interp1(tOld',dat_s,tNew');

           tmp  = dat_i(:,j);
           nanx = isnan(tmp);
           
           if flag && any(nanx)
               tmp  = dat_i(:,j);
               nanx = isnan(tmp);
               % Interpolate only if just a few samples are missing
               if sum(nanx)<nSamp*fracNaN
                   t             = 1:numel(tmp);
                   dat_i(nanx,j) = interp1(t(~nanx), tmp(~nanx), t(nanx), ...
                                            'linear','extrap');                                         
               end
           end
        
        end
            
    end
  
end

